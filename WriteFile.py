
#write data to file
#obj = open("C:\\Users\\praveen.sunkara\\Documents\\obj.txt", 'w')
# obj.write("This is our first line")
# obj.close()

#append data to an existing file
# obj = open("C:\\Users\\praveen.sunkara\\Documents\\obj.txt", 'a')
# obj.write("\nthis is the appended data")

#methods available with file
obj = open("C:\\Users\\praveen.sunkara\\Documents\\obj.txt", 'r')

#tell method diplays the position of the cursor in the file
print(obj.tell())
obj.readline()
print(obj.tell())

#seek methods allows to navigate the cursor position to desired position
obj.seek(10)
print(obj.tell())